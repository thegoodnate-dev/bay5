export const state = () => (
  [
    {
      name: "Breakfast",
      id: "breakfast",
      imageDescription: "Continental Breakfast Slate",
      description: ["Our breakfast menu is available all day, every day!"],
      options: [
        {
          name: "yoghurt bowl",
          description: "with fresh summer berries & optional golden honey.",
          v: true,
          ve: false,
          gf: true,
          newUntil: new Date('2020-11-01')
        },
        {
          image: "/images/avocado-toast.jpg",
          imageDescription: "Image of three smashed avocado on toast with various toppings."
        },
        {
          name: "Smashed Avocado Toast",
          description: "Topped with either crispy bacon, vine ripe tomatoes or halloumi.",
          v: true,
          ve: true,
          gf: false,
          recommended: false,
          newUntil: new Date('2020-11-01')
        },
        {
          name: "Trio of Pastries",
          description: "Freshly baked mini pastries.",
          v: true,
          ve: false,
          gf: false
        },
        {
          name: "Toast",
          description: "2 slices of toast served with butter. Add strawberry preserve or marmalade as a little extra.",
          v: true,
          ve: true,
          gf: false
        },
        {
          name: "Toasted Teacake",
          description: "Served with butter. Add strawberry preserve or marmalade as a little extra.",
          v: true,
          ve: false,
          gf: false
        },
        {
          name: "Bacon Sandwich",
          description: "A generous stack of crispy bacon served in your choice of bloomer (white or brown) or ciabatta.",
          v: false,
          ve: false,
          gf: false,
          recommended: true
        },
        {
          image: "/images/continental-breakfast.jpg",
          imageDescription: "Image of two continental breakfast slates."
        },
        {
          name: "Continental Breakfast Slate",
          description: "The ultimate Bay 5 breakfast experience... Mini pastries, toast with butter & preserve, \
            cheese, charcuterie meat, fresh berries with yogurt and \
            a glass of fresh orange juice.",
          v: false,
          ve: false,
          gf: false
        }
      ]
    },
    {
      name: "Beer & Cider",
      id: "beer-and-cider",
      imageDescription: "Peroni",
      options: [
        {
          name: "Peroni",
        },
        {
          name: "San Miguel",
        },
        {
          name: "Recordelig Cider"
        }
      ]
    },
    {
      name: "Crêpes",
      id: "crepes",
      imageDescription: "Nutella with Strawberries",
      description: [
        "Treat Yourself! Add a scoop of award winning Wild Fig artisan ice cream to any of our sweet crêpes."
      ],
      options: [
        {
          name: "Traditional",
          description: "Served the traditional French way... covered in sugar & melted butter.",
          v: false,
          ve: false,
          gf: false
        },
        {
          image: "/images/traditional-crepe.jpg",
          imageDescription: "Image of a traditional crepe."
        },
        {
          name: "Honey & Fresh Banana",
          v: true,
          ve: false,
          gf: false
        },
        {
          name: "Cinnamon & Cream Cheese Frosting",
          description: "Dusted in brown sugar.",
          v: true,
          ve: false,
          gf: false
        },
        {
          image: "/images/nutella-crepe.jpg",
          imageDescription: "Image of a crepe covered in Nutella, served with banana slices."
        },
        {
          name: "Nutella",
          description: "Also available with strawberries.",
          v: true,
          ve: false,
          gf: false
        },
        {
          name: "Salted Caramel & Banana",
          description: "Or try our Salted Caramel & Walnuts option.",
          v: true,
          ve: false,
          gf: false,
          recommended: true
        },
        {
          name: "Summer Berries",
          description: "With sprinkled sugar & marscopone or maple syrup.",
          v: true,
          ve: false,
          gf: false
        },
        {
          image: "/images/summer-berries.png",
          imageDescription: "Image of a summer berries crepe being drizzled with maple syrup."
        },
        {
          name: "Carved Ham & Cheddar Cheese"
        },
        {
          name: "Mozzarella with Vine Tomato & Basil",
          v: true,
          ve: false,
          gf: false
        },
        {
          name: "Cheddar Cheese & Tomato",
          v: true,
          ve: false,
          gf: false
        },
        {
          name: "Crispy Bacon & Maple Syrup"
        }
      ]
    },
    {
      name: "Ice Cream",
      id: "ice-cream",
      imageDescription: "Wild Fig Ice Cream",
      description: ["Award winning artisan ice cream made locally\
        by Lucy in Peterston-Super-Ely!"],
      options: [
        {
          name: "Single / Double Tub"
        },
        {
          name: "Traditional Wafer Cone"
        },
        {
          name: "Sugar Cone"
        },
        {
          name: "Chocolate Dipped Cone"
        },
        {
          name: "Double Waffle Cone"
        },
        {
          name: "Ice Lollies",
          description: "A selection of packaged ice lollies, ice creams and cornets are\
            available during the Spring and Summer seasons."
        }
      ]
    },
    {
      name: "Pancake Stacks",
      id: "pancakes",
      imageDescription: "The Very Berry One",
      description: [
        "With a new recipe for fluffier pancakes and plenty to choose from, there has never been a better\
           time to treat yourself!"
      ],
      options: [
        {
          name: "The Bay 5 One",
          description: "Our signature stack packed with fresh banana, salted caramel sauce\
              and a loving scoop of award winning vanilla ice cream... Are you ready for a taste explosion?!",
          v: true,
          ve: false,
          gf: false,
          recommended: true
        },
        {
          image: "/images/dreamy-one.jpg",
          imageDescription: "Image of three fluffy stacked pancakes topped with chocolate ice cream and strawberries drizzled with chocolate sauce."
        },
        {
          name: "The Dreamy One",
          description: "Lashings of Nutella & fresh strawberries topped with more of the same\
              and a mountain of award winning creamy chocolate ice cream... And more Nutella of course!\
              You will dream sweetly after this one!",
          v: true,
          ve: false,
          gf: false
        },
        {
          name: "The Cinnamon One",
          description: "Layered pancake stack with cream cheese frosting, cinnamon & brown sugar\
              topped with fresh banana and a generous scoop of banana and salted caramel ice cream!",
          v: true,
          ve: false,
          gf: false
        },
        {
          name: "The New Golden One",
          description: "Layered peanut butter pancakes topped with fresh banana chunks and Rowse golden\
              honey for pouring... Like sunshine on a rainy day!",
          v: true,
          ve: false,
          gf: false
        },
        {
          image: "/images/very-berry-one.jpg",
          imageDescription: "Image of the 'very berry one' pancake stack."
        },
        {
          name: "The Very Berry One",
          description: "3 fluffy pancakes topped with fresh summer berries, a sprinkle of sugar and a side of Canadian maple syrup.",
          v: true,
          ve: false,
          gf: false
        },
        {
          name: "The Classic One",
          description: "A stack of 3 fluffy pancakes cooked with fresh blueberries, topped with melted butter\
              and a side of Canadian maple syrup.. Melt in your mouth kinda stuff!",
          v: true,
          ve: false,
          gf: false
        },
        {
          name: "The Indulgent One",
          description: "Stack of pancakes layered with chocolate sauce and piled high with crumbed Cadbury's chocolate flakes,\
              a generous scoop of award winning chocolate ice cream, flake, lashings of chocolate sauce and a ball of white chocolate\
              to break it up a bit! Chocolate lovers heaven right there!",
          v: true,
          ve: false,
          gf: false
        },
        {
          image: "/images/american-one.jpg",
          imageDescription: "Image of a pancake stack topped with crispy bacon."
        },
        {
          name: "The American One",
          description: "3 fluffy pancakes topped with a stack of hot crispy bacon pieces and a side of\
              Canadian mayple syrup... Delish!"
        }
      ]
    },
    {
      name: "Salad & Soup",
      id: "salads",
      imageDescription: "Takeaway Salad",
      description: ["Our salads are made fresh in store each day."],
      options: [
        {
          name: "Tuna, Olive & Red Onion",
          description: "Served with salad cream and optional olives.",
          v: false,
          ve: false,
          gf: true,
          recommended: true
        },
        {
          image: "/images/tuna-salad2.png",
          imageDescription: "Image of the tuna, oliva and red onion salad."
        },
        {
          name: "Carved Ham & Hard Boiled Egg",
          description: "Served with a pot of salad cream or pickle.",
          v: false,
          ve: false,
          gf: true
        },
        {
          name: "Caprese",
          description: "Mozzarella, vine tomatoes & basil on a bed of greens with ciabatta.",
          v: true,
          ve: false,
          gf: false
        },
        {
          image: "/images/soup.png",
          imageDescription: "Image of two bowls of homemade soup with ciabatta slices on the side."
        },
        {
          name: "Fresh Soup",
          description: "soup of the day served with ciabatta slices and real butter.",
          v: false,
          ve: false,
          gf: false,
          recommended: false
        }
      ]
    },
    {
      name: "Sandwiches & Paninis",
      id: "sandwiches",
      imageDescription: "Carved Ham & Cheddar Cheese Panini",
      description: [
        "Hot or cold, we have the sandwich for you!"
      ],
      options: [
        {
          name: "Carved Ham Salad Ciabatta",
          description: "Served with with Hellman's mayonnaise or Heinz salad cream."
        },
        {
          image: "/images/ham-ciabatta.png",
          imageDescription: "Image of a ciabatta roll burting with carved ham and salad."
        },
        {
          name: "Cheddar Cheese & Vine Tomato Ciabatta",
          description: "Served with Branston pickle.",
          v: true,
          ve: false,
          gf: false
        },
        {
          name: "Cheese & Ham Toastie",
          description: "Carved ham, cheese and tomato on bloomer bread with a side of crisps.",
          v: false,
          ve: false,
          gf: false
        },
        {
          name: "Carved Ham & Cheddar Cheese Panini",
          description: "Served with crisps and side salad with balsamic glaze."
        },
        {
          name: "Mozzarella, Vine Tomato & Basil Panini",
          description: "Optionally add pesto. Served with crisps and side salad with balsamic glaze.",
          v: true,
          ve: false,
          gf: false,
          recommended: true
        },
        {
          image: "/images/sandwich-selection.png",
          imageDescription: "Image of various bay 5 sandwiches and paninis beautifully laid out on a table."
        },
        {
          name: "Tuna Melt Panini",
          description: "Served with crisps and side salad with balsamic glaze."
        }
      ]
    },
    {
      name: "Sharing Boards",
      id: "sharing-boards",
      imageDescription: "Mediterranean Board",
      description: [
        "Low gluten and ve options available."
      ],
      options: [
        {
          name: "Mediterranean Board",
          description: "Warm pitta slices served with grilled halloumi,\
            charcuterie meat, olives, hummus, stuffed pepper, fresh cucumber, rocket\
            and fresh lemon",
          v: false,
          ve: false,
          gf: false,
          recommended: true
        },
        {
          image: "/images/falafel-board.jpg",
          imageDescription: "Image of the falafel sharing board."
        },
        {
          name: "Falafel Board",
          description: "A spiced snack made from a blend of spicy sweet potato,\
            carrot, chick peas apricots with coriander leaf served with warm pitta\
            bread, olives, hummus, fresh cucumber, rocket, mango chutney and fresh lemon.",
          v: true,
          ve: true,
          gf: false
        },
        {
          image: "/images/meat-sharer.png",
          imageDescription: "Image of the deli meat and welsh cheese sharing board."
        },
        {
          name: "Deli Meat & Welsh Cheese Sharer",
          description: "A fine selection of charcuterie meats and welsh cheeses displayed on a bed of\
            rocket and served with artisan crackers, parma wrapped sesame bread\
            sticks, Welsh chilli jam, pickles and ciabatta slices - Perfect for two to share."
        },
        {
          name: "Ploughman's Board",
          description: "Carved ham and welsh cheese served with a hard\
            boiled egg, ciabatta slices, piper crisps, Branston pickle, fresh grapes or\
            sliced apple and pickled onions."
        },
        {
          image: "/images/ploughmans-board.jpg",
          imageDescription: "Image of the ploughmans sharing board."
        }
      ]
    },
  ]
)