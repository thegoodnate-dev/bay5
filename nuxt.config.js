export default {
  mode: 'universal',
  head: {
    htmlAttrs: {
      lang: 'en'
    },
    title: 'Bay 5 Coffee House',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
    ]
  },
  loading: true,
  css: [
    '~/assets/css/main.scss'
  ],
  plugins: [
    { src: '~/plugins/vue-scroll-reveal', ssr: false }
  ],
  buildModules: [
  ],
  modules: [
    'bootstrap-vue/nuxt',
    'nuxt-fontawesome',
    'nuxt-imagemin'
  ],
  fontawesome: {
    imports: [
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: [
          'faAngleRight',
          'faAngleLeft',
          'faBars',
          'faTimes'
        ]
      },
      {
        set: '@fortawesome/free-brands-svg-icons',
        icons: [
          'faTwitterSquare',
          'faFacebookSquare',
          'faInstagram'
        ]
      }
    ]
  },
  bootstrapVue: {
    componentPlugins: [
      'LayoutPlugin',
      'NavbarPlugin',
      'ButtonPlugin',
      'JumbotronPlugin',
      'ImagePlugin',
      'CarouselPlugin',
      'CardPlugin'
    ]
  },
  generate: {
    routes: [
      '/menu/breakfast',
      '/menu/afternoon-tea',
      '/menu/beer-and-cider',
      '/menu/crepes',
      '/menu/ice-cream',
      '/menu/pancakes',
      '/menu/salads',
      '/menu/sandwiches',
      '/menu/sharing-boards',
    ]
  }
}
